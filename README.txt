Creeper
-------
Wrapper for the AWS legacy SDK.

Configuration
-------------
* Obtain the latest AWS SDK for PHP from:
  http://pear.amazonwebservices.com/get/sdk-1.2.3.zip
  Unzip it into the creeper directory.

  At this step there should be a "sdk-1-x-y" directory inside the creeper
  directory and inside this directory another "sdk-1-x-y". Rename this
  subdirectory "sdk" and delete the archive file file.

* Move the "sdk" subdirectory to the root of the creeper directory and
  erase the unwanted "sdk-x-y" directory.

* Move the "config.inc.php" file into the "sdk" directory.

* Install the module via the admin/modules page.

* Go to the "AWS SDK for PHP" settings page (admin/config/system/creeper).

* Fill in the AWS account settings.

* The AWS SDK for PHP API can now be used in other modules that depend upon
  the creeper module or in custom modules that require the AWS SDK.

* API docs are available at http://http://aws.amazon.com/sdkforphp/ 
